chrome.runtime.onMessage.addListener( message =>
    {
        if( message?.browserAction?.badge?.color ) {
            chrome.browserAction.setBadgeBackgroundColor( {
                    color: String( message.browserAction.badge.color ),
                });
        }
        if( message?.browserAction?.badge?.text ) {
            chrome.browserAction.setBadgeText( {
                    text: String( message.browserAction.badge.text ),
                });
        }
        if( message?.browserAction?.title ) {
            chrome.browserAction.setTitle( {
                    title: String( message.browserAction.title ),
                });
        }
    });


chrome.browserAction.onClicked.addListener( tab =>
    tab.url !== 'https://www.youtube.com/'
    ? null
    : chrome.tabs.executeScript(
        null,
        {
            file: 'intab.js',
        }));
