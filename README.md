# Trash YouTube Recommendations


**WORKS ONLY WHEN YOUTUBE LANGUAGE IS SET TO ENGLISH**


TL;DR a Chrome extension that trashes your Youtube recommendations: it marks all of them as "Not interested", which forces Youtube to give you new ones.


**WORKS ONLY WHEN YOUTUBE LANGUAGE IS SET TO ENGLISH**


Youtube recommendations keep showing the same videos over and over and over and over and over and over again (bug reported here: https://support.google.com/youtube/thread/8407114?hl=en ), which is totally pointless for a service which has virtually infinite videos available in their library. The Fear Of Missing Out is non-existant when each and every event, opinion, belief, idea in the whole wide world has already been covered by thousands of distinct videos on Youtube. If i scroll by a recommended video from Alice, Youtube shouldnt try to shove that down my throat, but recommend me a video of the same subject from Bob.

Also, why are videos that i have already watched in my recommended feed? Why would i watch the same video twice?! And if, for some reason i want to, that is what "History" is for. (bug reported here: https://support.google.com/youtube/thread/4560923?hl=en )

These two behaviours makes your recommended page stale, makes you keep refreshing, but no new content appears (btw exact same applies to Google News). So, this is why i wrote this very crude extension for myself. It just marks every video currently displayed on your Youtube main page as "Not interested", so Youtube should never recommend them again.

Permissions required:
- Read and change you data on youtube.com

In store at https://chrome.google.com/webstore/detail/dkomodiphnbnnfaokdokdjejjgafoffi


Open source at https://gitlab.com/phpmoli/trash-recommendations/ The source code has only 15 lines of actual code and can be reviewed in max 5 minutes. It is written very poorly, for example it uses timers instead of mutationObservers. It is in alpha. I am open to pull requests.


**WORKS ONLY WHEN YOUTUBE LANGUAGE IS SET TO ENGLISH**


![screenshot](https://gitlab.com/phpmoli/trash-recommendations/raw/master/screenshot.png)


**WORKS ONLY WHEN YOUTUBE LANGUAGE IS SET TO ENGLISH**
