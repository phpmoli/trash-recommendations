const buttonSelector = `
        ytd-app ytd-page-manager ytd-browse ytd-rich-grid-renderer ytd-rich-item-renderer ytd-rich-grid-video-renderer:not([is-dismissed]) ytd-menu-renderer yt-icon-button button
        , ytd-app ytd-page-manager ytd-browse ytd-rich-grid-renderer ytd-rich-item-renderer ytd-rich-grid-media:not([is-dismissed]) ytd-menu-renderer yt-icon-button button
    `;

const notInterestedSelector = `
        ytd-app ytd-popup-container iron-dropdown ytd-menu-popup-renderer paper-listbox ytd-menu-service-item-renderer
        , ytd-app ytd-popup-container iron-dropdown ytd-menu-popup-renderer tp-yt-paper-listbox ytd-menu-service-item-renderer
        , ytd-app ytd-popup-container tp-yt-iron-dropdown ytd-menu-popup-renderer paper-listbox ytd-menu-service-item-renderer
        , ytd-app ytd-popup-container tp-yt-iron-dropdown ytd-menu-popup-renderer tp-yt-paper-listbox ytd-menu-service-item-renderer
    `;

( async () => {
    let count = 0;

    for( const buttonNode of [ ...window.document.querySelectorAll( buttonSelector )]) {
        buttonNode.click();
        await new Promise( resolve => setTimeout( resolve, 100 ));

        const notInterestedNode = [ ...window.document.querySelectorAll( notInterestedSelector )].filter( node => node.textContent.trim() === 'Not interested' ).find( node => node );
        if( notInterestedNode ) {
            notInterestedNode.click();
            await new Promise( resolve => setTimeout( resolve, 100 ));

            count++;
        }
    }
})();
